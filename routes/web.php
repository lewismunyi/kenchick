<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});
Route::get('/login', function () {
    return view('login');
});
Route::get('/signup', function () {
    return view('signup');
});

Route::get('/feeds', 'FeedController@index');

Route::get('/batches', 'BatchController@index')->name('batches.index');

Route::post('/batches', 'BatchController@store')->name('batches.store');

Route::get('/immunization', 'ImmunizationController@index')->name('immunizations.index');

//Change immunization status
Route::get('/immunization/{immunization}/{batch}', 'ImmunizationController@update')->name('vaccination.status');

Route::get('/dashboard', function () {
    return view('dashboard');
});
