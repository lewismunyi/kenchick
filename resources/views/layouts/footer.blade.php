<!-- Footer -->
<footer class="footer">
    <div class="row align-items-center justify-content-xl-between">
        <div class="col-xl-6">
            <div class="copyright text-center text-xl-left text-muted">
                <script>document.write("&copy;" + new Date().getFullYear());</script> <a href="#" class="font-weight-bold ml-1" target="_blank">{{config('app.name', 'Kuku App')}}</a>
            </div>
        </div>
        <div class="col-xl-6">
            <ul class="nav nav-footer justify-content-center justify-content-xl-end">
                <li class="nav-item">
                    <a href="#" class="nav-link" target="_blank">Home</a>
                </li>
                <li class="nav-item">
                    <a href="https://github.com/lewis-munyi/kenchick/blob/master/LICENSE.md" class="nav-link" target="_blank">GNU GPL-3.0</a>
                </li>
            </ul>
        </div>
    </div>
</footer>
