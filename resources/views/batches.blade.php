@extends('layouts.app')
@section('nav-name')
    <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="./index.html">Poultry Batches</a>
@endsection

@section('content')
    @include('header')
    <!-- Page content -->
    <div class="container-fluid mt--7">
        <div class="row mt-5">
            <div class="col-xl-8 mb-0 mb-xl-0">
                <div class="card shadow">
                    <div class="card-header border-0 bg-p">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0 text-center">All Batches</h3>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <!-- Projects table -->
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">Batch Number</th>
                                <th scope="col">Number</th>
                                <th scope="col">Hatchling</th>
                                <th scope="col">Immunized</th>
                                <th scope="col">Age (Weeks)</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($batches as $batch)
                                <tr>
                                    <td>{{$batch->id}}</td>
                                    <td>{{$batch->count}}</td>
                                    <td>{{$batch->type}}</td>
                                    <td>
                                    <span class="badge badge-dot mr-4">
                                        Yes <i class="bg-success"></i>
                                      </span>
                                    </td>
                                    <td>{{$batch->age}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer">
                    {{$batches->links()}}
                    </div>
                </div>
            </div>
            <div class="col-xl-4">
                <div class="card shadow border-0">
                    <div class="card-header border-0 bg-dark">
                        <div class="row align-items-center">
                            <div class="col text-center">
                                <h3 class="mb-0 text-white">Add new batch</h3>
                            </div>
                        </div>
                    </div>
                    <div class="card-body px-lg-5 py-lg-5">
                        <form method="POST" action="{{route('batches.store')}}" role="form">
                        @csrf
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="birdtype">Select hatchling</label>
                                        <div class="input-group input-group-alternative">
                                            <select name="type" class="custom-select" required>
                                                <option selected>Hatchling</option>
                                                <option value="chicken">Chicken</option>
                                                <option value="duckling">Duck</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="numberofchicks">Number of Chicks</label>
                                        <div class="input-group input-group-alternative bg-white">
                                            <input name="count" class="form-control" placeholder="1000" type="number" id="numberofchicks" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="dateofhatching">Date of hatching</label>
                                        <div class="input-group input-group-alternative">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                                            </div>
                                            <input name="hatch_date" class="form-control datepicker" placeholder="Select date" id="dateofhatching" type="text" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary my-4">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footer')
    </div>
@endsection
