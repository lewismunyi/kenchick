@extends('layouts.app')
@section('nav-name')
    <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="./index.html">Immunizations</a>
@endsection

@section('content')
    <!-- Header -->
    @include('header')
    <!-- Page content -->
    <div class="container-fluid mt--7">
    @foreach($immunizations as $immunization)
        <!-- Table -->
        <div class="row mt-5">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <h3 class="mb-0">Diseases: @foreach($immunization->diseases as $disease) {{$disease->name}} @endforeach | Application: {{$immunization->method}}</h3>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">Batch Number</th>
                                    <th scope="col">Number of chicks</th>
                                    <th scope="col">Age (Weeks)</th>
                                    <th scope="col">Birth day</th>
                                    <th scope="col">Due Date</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Update Vaccination</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($immunization->batches as $batch)
                                <tr>
                                    <td>{{$batch->id}}</td>
                                    <td>{{$batch->count}}</td>
                                    <td>{{$batch->age}}</td>
                                    <td>{{$batch->hatch_date}}</td>
                                    <td>20/20/2019</td>
                                    <td class="d-flex justify-content-between">
                                    @if($batch->pivot->status == "1")
                                    <span class="badge badge-dot mr-4">
                                        <i class="bg-success"></i>Vaccinated
                                    </span>
                                    @else
                                    <span class="badge badge-dot mr-4">
                                        <i class="bg-warning"></i> Not Vaccinated
                                      </span>
                                    @endif
                                    </td>
                                    <td>
                                        @if($batch->pivot->status == "0")
                                        @method('PUT')
                                        @csrf
                                            <a class="btn btn-primary" href="{{route('vaccination.status', [$immunization->id, $batch->id])}}"><i class="ni ni-check-bold text-success"></i>Vaccinated</a>
                                            <!-- <a class="dropdown-item" href="#"><i class="ni ni-fat-remove text-danger"></i>Not vaccinated</a> -->
                                        @else
                                        <span class="badge badge-success">Already Vaccinated</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer py-4">
                        <nav aria-label="...">
                            <ul class="pagination justify-content-end mb-0">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1">
                                        <i class="fas fa-angle-left"></i>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                </li>
                                <li class="page-item active">
                                    <a class="page-link" href="#">1</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#">
                                        <i class="fas fa-angle-right"></i>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    @include('layouts.footer')
    </div>
    @endsection

@section('js')
    <script type="text/javascript">
        $('body').on('click', '.status', function(e) {
        e.preventDefault();

        $.ajax({
            type: 'GET',
            url: $(this).attr('href'),
            success: function(data) {
                console.log(data);
            },
            error: function(err) {
                console.log(err);
            },
        });
    });
    </script>
@endsection