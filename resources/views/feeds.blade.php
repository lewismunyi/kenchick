@extends('layouts.app')
@section('nav-name')
    <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="./index.html">Poultry Feeds</a>
@endsection
@section('content')
    @include('header')
    <!-- Page content -->
    <div class="container-fluid mt--7">
        <!-- Table -->
        <div class="row mt-5">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <h3 class="mb-0">Chick Mash | Duck Mash</h3>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">Batch Number</th>
                                <th scope="col">Number of chicks</th>
                                <th scope="col">Age (Weeks)</th>
                                <th scope="col">Exp. Average weight</th>
                                <th scope="col">gms/bird/day</th>
                                <th scope="col">Total feed weight(Kgs)</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>123</td>
                                <td>3000</td>
                                <td>5</td>
                                <td>300-500</td>
                                <td>36</td>
                                <td>0.14</td>
                            </tr>
                            <tr>
                                <td>123</td>
                                <td>3000</td>
                                <td>5</td>
                                <td>300-500</td>
                                <td>36</td>
                                <td>0.14</td>
                            </tr>
                            <tr>
                                <td>123</td>
                                <td>3000</td>
                                <td>5</td>
                                <td>300-500</td>
                                <td>36</td>
                                <td>0.14</td>
                            </tr>
                            <tr>
                                <td>123</td>
                                <td>3000</td>
                                <td>5</td>
                                <td>300-500</td>
                                <td>36</td>
                                <td>0.14</td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer py-4">
                        <nav aria-label="...">
                            <ul class="pagination justify-content-end mb-0">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1">
                                        <i class="fas fa-angle-left"></i>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                </li>
                                <li class="page-item active">
                                    <a class="page-link" href="#">1</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#">
                                        <i class="fas fa-angle-right"></i>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- Table -->
        <div class="row mt-5">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <h3 class="mb-0">Growers Mash</h3>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">Batch Number</th>
                                <th scope="col">Number of chicks</th>
                                <th scope="col">Age (Weeks)</th>
                                <th scope="col">Exp. Average weight</th>
                                <th scope="col">gms/bird/day</th>
                                <th scope="col">Total feed weight(Kgs)</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>123</td>
                                <td>3000</td>
                                <td>5</td>
                                <td>300-500</td>
                                <td>36</td>
                                <td>0.14</td>
                            </tr>
                            <tr>
                                <td>123</td>
                                <td>3000</td>
                                <td>5</td>
                                <td>300-500</td>
                                <td>36</td>
                                <td>0.14</td>
                            </tr>
                            <tr>
                                <td>123</td>
                                <td>3000</td>
                                <td>5</td>
                                <td>300-500</td>
                                <td>36</td>
                                <td>0.14</td>
                            </tr>
                            <tr>
                                <td>123</td>
                                <td>3000</td>
                                <td>5</td>
                                <td>300-500</td>
                                <td>36</td>
                                <td>0.14</td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer py-4">
                        <nav aria-label="...">
                            <ul class="pagination justify-content-end mb-0">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1">
                                        <i class="fas fa-angle-left"></i>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                </li>
                                <li class="page-item active">
                                    <a class="page-link" href="#">1</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#">
                                        <i class="fas fa-angle-right"></i>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- Table -->
        <div class="row mt-5">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <h3 class="mb-0">Layers Mash</h3>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">Batch Number</th>
                                <th scope="col">Number of chicks</th>
                                <th scope="col">Age (Weeks)</th>
                                <th scope="col">Exp. Average weight</th>
                                <th scope="col">gms/bird/day</th>
                                <th scope="col">Total feed weight(Kgs)</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>123</td>
                                <td>3000</td>
                                <td>5</td>
                                <td>300-500</td>
                                <td>36</td>
                                <td>0.14</td>
                            </tr>
                            <tr>
                                <td>123</td>
                                <td>3000</td>
                                <td>5</td>
                                <td>300-500</td>
                                <td>36</td>
                                <td>0.14</td>
                            </tr>
                            <tr>
                                <td>123</td>
                                <td>3000</td>
                                <td>5</td>
                                <td>300-500</td>
                                <td>36</td>
                                <td>0.14</td>
                            </tr>
                            <tr>
                                <td>123</td>
                                <td>3000</td>
                                <td>5</td>
                                <td>300-500</td>
                                <td>36</td>
                                <td>0.14</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer py-4">
                        <nav aria-label="...">
                            <ul class="pagination justify-content-end mb-0">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1">
                                        <i class="fas fa-angle-left"></i>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                </li>
                                <li class="page-item active">
                                    <a class="page-link" href="#">1</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#">
                                        <i class="fas fa-angle-right"></i>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footer')
    </div>
@endsection
