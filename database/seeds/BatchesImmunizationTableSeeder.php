<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Immunization;
use App\Batch;

class BatchesImmunizationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $immunizations = Immunization::all();
        
        Batch::all()->each(function ($batch) use ($immunizations) { 
            $batch->immunizations()->saveMany($immunizations);
        });
    }
}
