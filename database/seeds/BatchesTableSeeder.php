<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Batch;
use Carbon\Carbon;

class BatchesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Chicks
        Batch::create([
            'count' => '3000',
            'feed_id' => '1',
            // 'hatch_date' => '20/01/2019',
            'hatch_date' => Carbon::parse('2019-02-20'),
            'type' => 'chicken'
        ]);

        Batch::create([
            'count' => '3000',
            'feed_id' => '1',
            // 'hatch_date' => '20/01/2019',
            'hatch_date' => Carbon::parse('2019-02-21'),
            'type' => 'chicken'
        ]);

        Batch::create([
            'count' => '3000',
            'feed_id' => '1',
            // 'hatch_date' => '20/02/2019',
            'hatch_date' => Carbon::parse('2019-02-22'),
            'type' => 'chicken'
        ]);

        Batch::create([
            'count' => '3000',
            'feed_id' => '1',
            // 'hatch_date' => '20/02/2019',
            'hatch_date' => Carbon::parse('2019-02-23'),
            'type' => 'duck'
        ]);

        //Growers
        Batch::create([
            'count' => '1500',
            'feed_id' => '2',
            // 'hatch_date' => '20/11/2019',
            'hatch_date' => Carbon::parse('2018-11-20'),
            'type' => 'duck'
        ]);

        Batch::create([
            'count' => '2000',
            'feed_id' => '2',
            // 'hatch_date' => '10/11/2019',
            'hatch_date' => Carbon::parse('2018-11-10'),
            'type' => 'duck'
        ]);

        Batch::create([
            'count' => '4000',
            'feed_id' => '2',
            // 'hatch_date' => '1/12/2019',
            'hatch_date' => Carbon::parse('2018-12-11'),
            'type' => 'chicken'
        ]);

        Batch::create([
            'count' => '3500',
            'feed_id' => '2',
            // 'hatch_date' => '15/11/2019',
            'hatch_date' => Carbon::parse('2018-11-15'),
            'type' => 'duck'
        ]);



        //Layers
        Batch::create([
            'count' => '1200',  
            'feed_id' => '3',
            // 'hatch_date' => '20/10/2019',
            'hatch_date' => Carbon::parse('2018-10-20'),
            'type' => 'chicken'
        ]);

        Batch::create([
            'count' => '500',
            'feed_id' => '3',
            // 'hatch_date' => '20/10/2019',
            'hatch_date' => Carbon::parse('2018-10-20'),
            'type' => 'chicken'
        ]);

        Batch::create([
            'count' => '800',
            'feed_id' => '3',
            // 'hatch_date' => '20/10/2019',
            'hatch_date' => Carbon::parse('2018-10-20'),
            'type' => 'chicken'
        ]);

        Batch::create([
            'count' => '1000',
            'feed_id' => '3',
            // 'hatch_date' => '20/10/2019',
            'hatch_date' => Carbon::parse('2018-10-20'),
            'type' => 'chicken'
        ]);

    }
}
