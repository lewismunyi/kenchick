<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DiseasesImmunizationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('disease_immunization')->insert([
            'disease_id' => '1',
            'immunization_id' => '1',
        ]);

        DB::table('disease_immunization')->insert([
            'disease_id' => '1',
            'immunization_id' => '2',
        ]);

        DB::table('disease_immunization')->insert([
            'disease_id' => '3',
            'immunization_id' => '2',
        ]);

        DB::table('disease_immunization')->insert([
            'disease_id' => '4',
            'immunization_id' => '3',
        ]);

        DB::table('disease_immunization')->insert([
            'disease_id' => '1',
            'immunization_id' => '4',
        ]);

        DB::table('disease_immunization')->insert([
            'disease_id' => '3',
            'immunization_id' => '4',
        ]);

        DB::table('disease_immunization')->insert([
            'disease_id' => '4',
            'immunization_id' => '5',
        ]);

        DB::table('disease_immunization')->insert([
            'disease_id' => '1',
            'immunization_id' => '6',
        ]);

        DB::table('disease_immunization')->insert([
            'disease_id' => '3',
            'immunization_id' => '6',
        ]);

        DB::table('disease_immunization')->insert([
            'disease_id' => '3',
            'immunization_id' => '7',
        ]);

        DB::table('disease_immunization')->insert([
            'disease_id' => '1',
            'immunization_id' => '7',
        ]);

        DB::table('disease_immunization')->insert([
            'disease_id' => '6',
            'immunization_id' => '8',
        ]);

        DB::table('disease_immunization')->insert([
            'disease_id' => '5',
            'immunization_id' => '9',
        ]);

        DB::table('disease_immunization')->insert([
            'disease_id' => '7',
            'immunization_id' => '10',
        ]);

        DB::table('disease_immunization')->insert([
            'disease_id' => '6',
            'immunization_id' => '11',
        ]);

        DB::table('disease_immunization')->insert([
            'disease_id' => '3',
            'immunization_id' => '12',
        ]);

        DB::table('disease_immunization')->insert([
            'disease_id' => '1',
            'immunization_id' => '12',
        ]);

        DB::table('disease_immunization')->insert([
            'disease_id' => '4',
            'immunization_id' => '12',
        ]);

        DB::table('disease_immunization')->insert([
            'disease_id' => '6',
            'immunization_id' => '12',
        ]);

        DB::table('disease_immunization')->insert([
            'disease_id' => '7',
            'immunization_id' => '13',
        ]);
    }
}
