<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UsersTableSeeder::class,
            DiseasesTableSeeder::class,
            ImmunizationsTableSeeder::class,
            DiseasesImmunizationTableSeeder::class,
            FeedsTableSeeder::class,
            BatchesTableSeeder::class,
            BatchesImmunizationTableSeeder::class
        ]);
    }
}
