<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Immunization;

class ImmunizationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //1
        Immunization::create([
            //'start_day' => '1',
            //'end_day' => '9',
            'method' => 'Intra muscular'
        ]);

        //2
        Immunization::create([
            //'start_day' => '1',
            //'end_day' => '9',
            'method' => 'spray'
        ]);

        //3
        Immunization::create([
            // 'start_day' => '10',
            // 'end_day' => '14',
            'method' => 'Drinking Water'
        ]);

        //4
        Immunization::create([
            // 'start_day' => '15',
            // 'end_day' => '18',
            'method' => 'Eye drop'
        ]);

        //5
        Immunization::create([
            // 'start_day' => '24',
            // 'end_day' => '28',
            'method' => 'Drinking Water'
        ]);

        //6
        Immunization::create([
            // 'start_day' => '29',
            // 'end_day' => '32',           
            'method' => 'Eye drop'
        ]);

        //7
        Immunization::create([
            // 'start_day' => '42',
            // 'end_day' => '56',
            'method' => 'Intra muscular or spray'
        ]);

        //8
        Immunization::create([
            // 'start_day' => '42',
            // 'end_day' => '56',
            'method' => 'Spray'
        ]);

        //9
        Immunization::create([
            // 'start_day' => '56',
            // 'end_day' => '70',           
            'method' => 'Wing stab'
        ]);

        //10
        Immunization::create([
            // 'start_day' => '56',
            // 'end_day' => '70',           
            'method' => 'Sub Cutaneous'
        ]);

        //11
        Immunization::create([
            // 'start_day' => '84',
            // 'end_day' => '98',
            'method' => 'Intra muscular'
        ]);

        //12
        Immunization::create([
            // 'start_day' => '112',
            // 'end_day' => '126',
            'method' => 'Intra muscular or spray'
        ]);

        //13
        Immunization::create([
            // 'start_day' => '112',
            // 'end_day' => '126',
            'method' => 'Sub Cutaneous'
        ]);
    }
}
