<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Disease;
class DiseasesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //1
        Disease::create([
            'name' => 'Infectious Bronchitis (IB)'
        ]);

        //2
        Disease::create([
            'name' => 'Mareks Disease'
        ]);

        //3
        Disease::create([
            'name' => 'Newcastle Disease(NCD)'
        ]);

        //4
        Disease::create([
            'name' => 'Infectious Bursal Disease (IBD/Gumboro)'
        ]);

        //5
        Disease::create([
            'name' => 'Fowl Pox'
        ]);

        //6
        Disease::create([
            'name' => 'Fowl Typhoid'
        ]);

        //7
        Disease::create([
            'name' => 'Fowl Cholera'
        ]);
    }
}
