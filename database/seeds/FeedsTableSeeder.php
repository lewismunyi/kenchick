<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Feed;

class FeedsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Feed::create([
            'feed_type' => 'Chick and Duck Mash',
            // 'average_live_weight' => '40-60',
        ]);

        Feed::create([
            'feed_type' => 'Growers Mash',
            // 'average_live_weight' => '95-120',
        ]);

        Feed::create([
            'feed_type' => 'Layers Mash',
            // 'average_live_weight' => '150-200',
        ]);

        // Feed::create([
        //     'age' => '4',
        //     'feed_type' => 'Chick and Duck Mash',
        //     'feed_consumption' => '31',
        //     'average_live_weight' => '220-300',
        // ]);

        // Feed::create([
        //     'age' => '5',
        //     'feed_type' => 'Chick and Duck Mash',
        //     'feed_consumption' => '36',
        //     'average_live_weight' => '380-400',
        // ]);

        // Feed::create([
        //     'age' => '6',
        //     'feed_type' => 'Chick and Duck Mash',
        //     'feed_consumption' => '41',
        //     'average_live_weight' => '470-500',
        // ]);

        // Feed::create([
        //     'age' => '7',
        //     'feed_type' => 'Chick and Duck Mash',
        //     'feed_consumption' => '45',
        //     'average_live_weight' => '560-600',
        // ]);

        // Feed::create([
        //     'age' => '8',
        //     'feed_type' => 'Chick and Duck Mash',
        //     'feed_consumption' => '49',
        //     'average_live_weight' => '650-690',
        // ]);

        // Feed::create([
        //     'age' => '9',
        //     'feed_type' => 'Growers Mash',
        //     'feed_consumption' => '52',
        //     'average_live_weight' => '740-780',
        // ]);

        // Feed::create([
        //     'age' => '10',
        //     'feed_type' => 'Growers Mash',
        //     'feed_consumption' => '60',
        //     'average_live_weight' => '830-870',
        // ]);

        // Feed::create([
        //     'age' => '11',
        //     'feed_type' => 'Growers Mash',
        //     'feed_consumption' => '70',
        //     'average_live_weight' => '920-960',
        // ]);

        // Feed::create([
        //     'age' => '12',
        //     'feed_type' => 'Growers Mash',
        //     'feed_consumption' => '75',
        //     'average_live_weight' => '1010-1059',
        // ]);

        // Feed::create([
        //     'age' => '13',
        //     'feed_type' => 'Growers Mash',
        //     'feed_consumption' => '80',
        //     'average_live_weight' => '1100-1140',
        // ]);

        // Feed::create([
        //     'age' => '14',
        //     'feed_type' => 'Growers Mash',
        //     'feed_consumption' => '85',
        //     'average_live_weight' => '1185-1230',
        // ]);

        // Feed::create([
        //     'age' => '15',
        //     'feed_type' => 'Growers Mash',
        //     'feed_consumption' => '92',
        //     'average_live_weight' => '1270-1320',
        // ]);

        // Feed::create([
        //     'age' => '16',
        //     'feed_type' => 'Layers Mash',
        //     'feed_consumption' => '100',
        //     'average_live_weight' => '1355-1410',
        // ]);

        // Feed::create([
        //     'age' => '17',
        //     'feed_type' => 'Layers Mash',
        //     'feed_consumption' => '100',
        //     'average_live_weight' => '1355-1410',
        // ]);

        // Feed::create([
        //     'age' => '18',
        //     'feed_type' => 'Layers Mash',
        //     'feed_consumption' => '107',
        //     'average_live_weight' => '1440-1500',
        // ]);

        // Feed::create([
        //     'age' => '19',
        //     'feed_type' => 'Layers Mash',
        //     'feed_consumption' => '120',
        //     'average_live_weight' => '1580-1680',
        // ]);

        // Feed::create([
        //     'age' => '20',
        //     'feed_type' => 'Layers Mash',
        //     'feed_consumption' => '120',
        //     'average_live_weight' => '1645-1750',
        // ]);
    }
}
