<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiseaseImmunizationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disease_immunization', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('disease_id');
            $table->integer('immunization_id');
            //$table->primary(['disease_id', 'immunization_id']);
            // $table->foreign('disease_id')->references('id')->on('diseases')->onDelete('cascade');
            // $table->foreign('immunization_id')->references('id')->on('immunizations')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('disease_immunization');
    }
}
