<?php

namespace App\Http\Controllers;

use App\Feed;
use App\Batch;
use Illuminate\Http\Request;
use Carbon\Carbon;

class FeedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $chicks = Batch::where('hatch_date', '>=', date('Y-m-d', strtotime('-56 days')))->orderBy('hatch_date', 'desc')->get();

        $growers =      Batch::where('hatch_date', '<=', date('Y-m-d', strtotime('-63 days')))
                        ->where('hatch_date', '>', date('Y-m-d', strtotime('-112 days')))
                        ->orderBy('hatch_date', 'desc')->get();

        $layers = Batch::where('hatch_date', '<=', date('Y-m-d', strtotime('-112 days')))
                        ->where('hatch_date', '>', date('Y-m-d', strtotime('-140 days')))
                        ->orderBy('hatch_date', 'desc')->get();

        $data = [
            'chicks' => $chicks,
            'growers' => $growers,
            'layers' => $layers
        ];

        //dd(Carbon::parse('2019-01-20')->diffInDays(Carbon::now()));
        dd($data);
        //return view('feeds')->with($data, 'data');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Feed  $feed
     * @return \Illuminate\Http\Response
     */
    public function show(Feed $feed)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Feed  $feed
     * @return \Illuminate\Http\Response
     */
    public function edit(Feed $feed)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Feed  $feed
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Feed $feed)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Feed  $feed
     * @return \Illuminate\Http\Response
     */
    public function destroy(Feed $feed)
    {
        //
    }
}
