<?php

namespace App\Http\Controllers;

use App\Batch;
use App\Immunization;
use Illuminate\Http\Request;
use Carbon\Carbon;

class BatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //if array1 array2 yes
        //not no
        
        //Show all batches with complete immunizations
        // $immunization = Immunization::first();

        // $batches = $immunization->batches()
        // ->wherePivot('status', '=', '0')
        // ->get()->toArray(); 
        
        //dd($batches);
        $batches = Batch::OrderBy('created_at', 'desc')->paginate(10);
        return view('batches', compact('batches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $batch = New Batch;
        $batch->type = $request->type;
        $batch->count = $request->count;
        $batch->hatch_date = Carbon::parse($request->hatch_date);
        //dd($batch);
        $batch->save();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Batch  $batch
     * @return \Illuminate\Http\Response
     */
    public function show(Batch $batch)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Batch  $batch
     * @return \Illuminate\Http\Response
     */
    public function edit(Batch $batch)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Batch  $batch
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Batch $batch)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Batch  $batch
     * @return \Illuminate\Http\Response
     */
    public function destroy(Batch $batch)
    {
        //
    }
}
