<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Immunization;
use App\Batch;

class ImmunizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $immunizations = Immunization::with(['diseases', 'batches'])->get();
        $batch = Immunization::first()->batches->first();
        // dd($batch->pivot->status);
        return view('immunizations', compact('immunizations'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Change the immunization status of a batch
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Immunization $immunization, Batch $batch)
    {
        //
        $batch = Batch::find($batch->id);
        $immunization = Immunization::find($immunization->id);
        $batch->immunizations()->updateExistingPivot($immunization, ['status' => '1']);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
