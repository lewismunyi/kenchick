<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{
    //one to many //batches
    public function batches()
    {
        return $this->hasMany('App\Batch');
    }

    public function scopeChicks($query)
    {
        
        return $query->whereHas('batches', function ($query) {
            return $query->where('age', '<=', 56);
        })->get();
    }

    public function scopeLayers($query)
    {
        return $query->whereHas('batches', function ($query) {
            return $query->where('age', '>=', 63)
                        ->where('age', '>', 112);
        })->get();       
    }

    public function scopeGrowers($query)
    {
        return $query->whereHas('batches', function ($query) {
            return $query->where('age', '>=', 63)
                        ->where('age', '>', 112);
        })->get();      
    }
}

// $chicks = Feed::with('batches')
// ->where('age', '<=', 56)
// ->get()
// ->toArray();

// $growers = Feed::with('batches')
// ->where('age', '>=', 63)
// ->where('age', '>', 112)
// ->get()
// ->toArray();

// $layers = Feed::with('batches')
// ->where('age', '>=', 112)
// ->where('age', '<', 140)
// ->get()
// ->toArray();

