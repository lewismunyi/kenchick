<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Batch extends Model
{
    /**
     * The attributes that should be appended to the JSON return.
     *
     * @var array
     */
    

    //many to one //feed
    public function feed()
    {
        return $this->belongsTo('App\Feed');
    }

    //many to many immunizations
    public function immunizations()
    {
        return $this->belongsToMany('App\Immunization');
    }

    public function status()
    {
        return $this->hasOne('App\BatchImmunization');
    }


    //assign attribute age
    public function getAgeAttribute()
    {
        $now = Carbon::now();
        return Carbon::parse($this->attributes['hatch_date'])->diffInWeeks($now);
    }

    // public function getStatusAttribute()
    // {
    //     //Get the pivot table
    //     $this->attributes['id'];
    //     $batch = Batch::find($batch->id);
    //     $immunization = Immunization::find($immunization->id);

    //     if($this->wherePivot)
    //     {

    //     }
    //     return "yes";

    //     //Fetch all subsequent status
    //     //where has all batch_immunizations with $this->id and status all = true

    //     //
    // }
}
