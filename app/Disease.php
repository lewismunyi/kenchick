<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Disease extends Model
{
    //Disease and Immunization many to many relationship
    public function immunizations()
    {
        return $this->belongsToMany('App\Immunization');
    }
}
