<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BatchImmunization extends Model
{
    //
    protected $table = 'batch_immunization';
}
