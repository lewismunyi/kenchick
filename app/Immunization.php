<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Immunization extends Model
{
    //Disease and Immunization many to many relationship
    public function diseases()
    {
        return $this->belongsToMany('App\Disease');
    }

    public function batches()
    {
        return $this->belongsToMany('App\Batch')
                        ->withPivot('status');
    }

}
